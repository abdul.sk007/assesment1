package com.student.model.dao;

import java.util.*;
import java.util.Map.Entry;

public class Test {
	public static void main(String[] args) {
		StudentDao sdao = new StudentDaoImpl();
		
		List<Student> studentList = sdao.getAllStudents();
		//studentList.forEach(s -> System.out.println(s));
		//Map<Integer,Student> map = new HashMap<>();
		
		Map<Integer,String> smap = new HashMap<Integer,String>();
		smap.put(33, "sam");
		smap.put(43, "ram");
		smap.put(23, "mark");
		smap.put(53, "jhon");
		smap.put(63, "nick");
		Set<Entry<Integer,String>> es = smap.entrySet();
		System.out.println("results as per marks");
		es.stream().sorted(Map.Entry.comparingByKey()).forEach(System.out::println);
		
		
		System.out.println("results as per names");
		es.stream().sorted(Map.Entry.comparingByValue()).forEach(System.out::println);
		
		//smap.forEach((mark,name)-> System.out.println(mark + ":" + name));
	

}
}
